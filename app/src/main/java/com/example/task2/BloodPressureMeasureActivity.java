package com.example.task2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gcssloop.widget.ArcSeekBar;

import java.util.Random;

public class BloodPressureMeasureActivity extends AppCompatActivity {

ArcSeekBar mArcSeekBar;
TextView textView1,textView2,txt3;
Button btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_pressure_measure);
         mArcSeekBar=findViewById(R.id.arc_seek_bar);
        TextView textView1=findViewById(R.id.tv_xyz2);
        TextView textView2=findViewById(R.id.tv_xyz4);
        TextView txt3=findViewById(R.id.tv_xyz7);
        btn=findViewById(R.id.calcl_press);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Random rand = new Random();
                int int_random = rand.nextInt(100);
                mArcSeekBar.setProgress(int_random);
                txt3.setText(String.valueOf(int_random));
                textView1.setText("80");
                textView2.setText("120");
            }
        });

    }



}