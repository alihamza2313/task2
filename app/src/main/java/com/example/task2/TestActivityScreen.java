package com.example.task2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;

import com.bigkoo.pickerview.MyOptionsPickerView;

import java.util.ArrayList;

public class TestActivityScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_screen);
        NumberPicker picker = (NumberPicker) findViewById(R.id.picker);
        picker.setMaxValue(5);
        String[] strs = {"Zero", "One", "Two", "Three", "Four", "Five" };
        picker.setDisplayedValues(strs);
        picker.setWrapSelectorWheel(true);
    }
}