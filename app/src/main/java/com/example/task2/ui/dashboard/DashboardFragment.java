package com.example.task2.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.task2.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;

    LineChart lineChart1,lineChart2;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        lineChart1=root.findViewById(R.id.linechat);
        lineChart2=root.findViewById(R.id.linechat2);

        lineChart1.setDragEnabled(true);
        lineChart1.setScaleEnabled(false);
        lineChart2.setDragEnabled(true);
        lineChart2.setScaleEnabled(false);
        ArrayList<Entry> yvalue=new ArrayList<>();

        yvalue.add(new Entry(0,60f));
        yvalue.add(new Entry(1,90f));
        yvalue.add(new Entry(2,100f));
        yvalue.add(new Entry(3,10f));
        yvalue.add(new Entry(4,30f));
        yvalue.add(new Entry(5,500f));
        yvalue.add(new Entry(6,60f));
        yvalue.add(new Entry(7,20f));

        LineDataSet set1=new LineDataSet(yvalue,"Data set1");
        set1.setFillAlpha(110);

        ArrayList<ILineDataSet> dataSets=new ArrayList<>();
        dataSets.add(set1);

        LineData data=new LineData(dataSets);
        lineChart1.setData(data);
        lineChart2.setData(data);
        return root;
    }
}