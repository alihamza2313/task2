package com.example.task2.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.task2.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    BottomSheetBehavior bottomSheetBehavior;
    TextView buttonOpenBottomSheet;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                new ViewModelProvider(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        buttonOpenBottomSheet = root.findViewById(R.id.button_open_bottom_sheet);

        final  View botomview = root.findViewById(R.id.bootmsheet);
        bottomSheetBehavior = BottomSheetBehavior.from(botomview);
        buttonOpenBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });


        return root;
    }
}